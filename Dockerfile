FROM registry.gitlab.com/saltspiro/salt-container:master

COPY extmods /var/cache/salt/master/extmods
COPY vagrant/saltstack/etc/master /etc/salt/master.d/spiro.conf
COPY vagrant/saltstack/salt /srv/salt

RUN salt-call --local state.apply spiro
