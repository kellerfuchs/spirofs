'''
Runners for handling spiro-managed saltenvs
'''
from __future__ import absolute_import, print_function, unicode_literals
import logging
from salt.ext import six


log = logging.getLogger(__name__)


def query_highstate(saltenv):
    """
    Return the list of minions that have tops in the given saltenv.

    If the given saltenv has changed, these minions might be effected.

    Note that this is generally useful and does not require the use of spirofs.
    """
    tops = __salt__['salt.execute']('*', 'state.show_top')
    return [
        mid
        for mid, envs in tops.items()
        if envs and saltenv in envs
    ]


def issue_token(projects=None):
    """
    Issue a deployment token for the given projects, or all projects if None
    """
    if isinstance(projects, six.string_types):
        projects = projects.split(',')
    return __utils__['spiro_auth.generate_token'](projects=projects)


def revoke_token(token):
    """
    Revokes the given token, making it invalid and disallowing it from accessing
    anything.
    """
    log.warning("TODO: Implement revoke_token")


def check_token(token, project=None):
    """
    Checks if the given token is valid for the given project. If not project is
    given, only check that the token is valid.
    """
    return __utils__['spiro_auth.check_token'](token, project)


def inspect_token(token):
    """
    Gets the information embedded in the given token in a human-readable format.
    """
    return __utils__['spiro_auth.inspect_token'](token)

