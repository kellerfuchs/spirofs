from __future__ import absolute_import, print_function, unicode_literals

import logging
import os
import hashlib
import contextlib
import tempfile
import shutil
try:
    from urllib import unquote, quote
except ImportError:
    from urllib.parse import unquote, quote

import salt.utils.files
import salt.utils.hashutils
import salt.utils.path
import salt.utils.yaml
from salt.ext import six

log = logging.getLogger(__name__)


def _fsdir(*bits):
    """
    Takes quoted arguments
    """
    return salt.utils.path.join(__opts__['cachedir'], 'spirofs', *bits)


class Deployment(object):
    """
    A single environment.
    """
    # FIXME: Replace percent encoding with something else

    def __init__(self, project, deployment):
        self.project = project
        self.deployment = deployment

    @property
    def root(self):
        """
        The root directory of this deployment.
        """
        return _fsdir(
            quote(self.project),
            quote(self.deployment),
        )

    @property
    def saltenv(self):
        """
        The saltenv for this deployment.
        """
        return "%s/%s" % (self.project, self.deployment)

    @property
    def synth_tops(self):
        """
        The name of the synthesized topfile (suitable for salt)
        """
        return __opts__['state_top']

    @property
    def real_tops(self):
        """
        The name of the spiro topfile
        """
        return 'spirotop.sls'

    @classmethod
    def from_env(cls, saltenv):
        """
        Factory: From a saltenv
        """
        if '/' not in saltenv:
            return

        # FIXME: Handle this in a more intelligent way
        parts = saltenv.rsplit('/', 1)
        self = cls(parts[0], parts[1])
        if self.exists():
            return self

    @classmethod
    def iterdeployments(cls):
        """
        Yield all Deployments
        """
        if not os.path.isdir(_fsdir()):
            return
        for p in os.listdir(_fsdir()):
            for d in os.listdir(_fsdir(p)):
                yield cls(unquote(p), unquote(d))

    def serialize(self):
        """
        Produce a JSON-suitable representation of this object
        """
        return [self.project, self.deployment]

    @classmethod
    def deserialize(cls, data):
        """
        Reverse of .serialize()
        """
        if data:
            return cls(*data)

    def exists(self, fn=None):
        """
        Check that this deployment actually exists in the filesystem.
        """
        if fn is None:
            return os.path.isdir(self.root)
        elif fn == self.synth_tops:
            return os.path.exists(self.path(self.real_tops))
        else:
            return os.path.exists(self.path(fn))

    def ensure_exists(self):
        """
        Create this deployment if it does not exist.
        """
        if not self.exists():
            os.makedirs(self.root)

    def contents(self):
        """
        Yield (name, 'f'|'d') for each filesystem entry (real or synthesized)
        """
        if not self.exists():
            return

        for path, dirs, files in salt.utils.path.os_walk(self.root):
            relpath = path[len(self.root):].strip('/')
            # FIXME: Return empty dirs
            yield relpath, 'd'
            for f in files:
                yield salt.utils.path.join(relpath, f, use_posixpath=True), 'f'

        if os.path.exists(self.path(self.real_tops)):
            yield self.synth_tops, 'f'

    def path(self, fname):
        """
        Returns the real filesystem path for the given item.

        Doesn't actually check the filesystem.
        """
        # Kinda ignoring the fact that salt paths are always / and
        # Windows would like \ because Windows will accept /
        return salt.utils.path.join(self.root, fname)

    def open(self, fname, mode='r'):
        """
        Returns a file.like object for the given itme.
        """
        if fname == self.synth_tops:
            return contextlib.closing(self.synthesize_topfile())
        else:
            return salt.utils.files.fopen(self.path(fname), mode)

    def _hash_text(self, data, form):
        blob = data.encode(__salt_system_encoding__)
        hash_type = getattr(hashlib, form, None)
        if hash_type is None:
            raise ValueError('Invalid hash type: {0}'.format(form))

        hash_obj = hash_type()
        hash_obj.update(blob)
        return hash_obj.hexdigest()

    def hash_file(self, fname, hashtype):
        """
        Returns a hash for the given item.
        """
        if fname == self.synth_tops:
            return self._hash_text(
                self.synthesize_topfile().getvalue(),
                hashtype,
            )
        else:
            return salt.utils.hashutils.get_hash(
                self.path(fname),
                hashtype,
            )

    def _load_top(self):
        """
        Loads this deployment's topfile, returning the data for this deployment
        """
        # TODO: Cross-env references
        try:
            f = self.open(self.real_tops)
        except (IOError, OSError):
            pass
        else:
            with f:
                yaml = salt.utils.yaml.safe_load(f)
            yield self.saltenv, yaml[self.deployment]

    def synthesize_topfile(self):
        """
        Generate top.sls file
        """
        envs = dict(self._load_top())
        f = six.StringIO()
        salt.utils.yaml.safe_dump(envs, f)
        f.seek(0)
        return f

    def replace_with(self, dirname):
        """
        Replace this deployment with the one referred to by dirname.

        After this, the old data is automatically cleaned up, and dirname is no 
        longer a valid path.
        """
        olddir = self.root
        newdir = tempfile.mktemp(prefix=self.deployment, dir=_fsdir(quote(self.project)))
        os.rename(olddir, newdir)
        os.rename(dirname, olddir)
        shutil.rmtree(newdir)


def list_deployments():
    """
    Returns a list of all known Deployments (as Deployment objects).
    """
    return Deployment.iterdeployments()


def from_env(saltenv):
    """
    Gets the Deployment for the given saltenv
    """
    return Deployment.from_env(saltenv)


def from_pd(project, deployment):
    """
    Gets the Deployment for the given project/deployment pair
    """
    return Deployment(project, deployment)


def deserialize(data):
    """
    Turns the results of Deployment.serialize() back into an object.
    """
    return Deployment.deserialize(data)


def mkbufferdir(project, deployment):
    """
    Creates a directory suitable for eventually giving to Deployment.replace_with().
    """
    dep = from_pd(project, deployment)
    dep.ensure_exists()
    return tempfile.mkdtemp(
        prefix=quote(deployment),
        dir=_fsdir(quote(project)),
    )

