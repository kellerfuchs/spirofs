import os
import pymacaroons
import random
import string
import logging
import json
import time

log = logging.getLogger(__name__)

IDENT_ALPHABET = string.digits + string.ascii_letters + string.punctuation

_key_cache = None

rand = random.SystemRandom()


def _auth_dir():
    d = os.path.join(__opts__['pki_dir'], 'spiro_auth')
    if not os.path.exists(d):
        os.makedirs(d)
    return d


def _getkey():
    global _key_cache
    if _key_cache is None:
        keyfile = os.path.join(_auth_dir(), 'macaroon-key')
        if os.path.exists(keyfile):
            with open(keyfile, 'rt') as kf:
                _key_cache = kf.read()
        else:
            _key_cache = ''.join(rand.choice(IDENT_ALPHABET) for _ in range(64))
            with open(keyfile, 'wt') as kf:
                kf.write(_key_cache)
    return _key_cache


def _genident():
    return ''.join(
        rand.choice(IDENT_ALPHABET)
        for _ in range(32)
    )


def _mkpredicate(kind, arg):
    return str(kind) + '=' + json.dumps(arg)


def _parsepredicate(txt):
    kind, arg = txt.split('=', 1)
    return kind, json.loads(arg)


def _check_predicates(pred, project=None):
    log.debug("Check predicate (proj=%r): %r", project, pred)
    kind, arg = _parsepredicate(pred)
    if kind == 'not-before':
        return time.time() > arg
    elif kind == 'not-after':
        return time.time() < arg
    elif kind == 'project-allow':
        return project in arg or project is None
    elif kind == 'project-deny':
        return project not in arg or porject is None
    else:
        log.info("Unkown predicate: %r", pred)
        return False


def generate_token(projects=None, lifespan=None):
    m = pymacaroons.Macaroon(
        identifier=_genident(),
        key=_getkey(),
    )

    m.add_first_party_caveat(_mkpredicate('not-before', time.time()))
    if lifespan is not None:
        m.add_first_party_caveat(_mkpredicate('not-after', time.time() + lifespan))
    if projects is not None:
        m.add_first_party_caveat(_mkpredicate('project-allow', projects))

    return m.serialize()


def check_token(token, project=None):
    """
    Checks if the given token is valid for the given project. If not project is
    given, only check that the token is valid.
    """
    try:
        m = pymacaroons.Macaroon.deserialize(token)
    except TypeError:  # Raised by b64decode()
        log.info("Invalid token (TypeError)")
        return False
    except pymacaroons.exceptions.MacaroonDeserializationException:
        log.info("Invalid token (deserialization)")
        return False

    v = pymacaroons.Verifier()
    v.satisfy_general(lambda p: _check_predicates(p, project))

    # TODO: Check m.identifier against {white,black}list
    
    try:
        isok = v.verify(m, _getkey())
    except pymacaroons.exceptions.MacaroonVerificationFailedException:
        log.info("Verification problem")
        # XXX: Do we want to return something more useful?
        return False
    else:
        return isok


def inspect_token(token):
    try:
        m = pymacaroons.Macaroon.deserialize(token)
    except pymacaroons.exceptions.MacaroonDeserializeException:
        return "(invalid)"

    return m.inspect()


def revoke_token(token):
    log.warning("TODO: Implement revoke_token")

